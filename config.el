;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Corentin Cam"
      user-mail-address "corentin.cam@easymile.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "FiraCode Nerd Font" :size 14 :weight 'light :width 'normal)
      doom-variable-pitch-font (font-spec :family "Linux Libertine" :size 14))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-monokai-machine)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(load! "funcs.el")

;; ============================= Mode associations =============================
(add-to-list 'auto-mode-alist '("\\.ipp\\'" . c-or-c++-mode))
(add-to-list 'auto-mode-alist '("\\.pu\\'" . plantuml-mode))

;; ============================= Package tweaks =============================
(after! evil
  (defalias 'evil-insert-state 'evil-emacs-state)
  (define-key evil-emacs-state-map (kbd "<escape>") 'evil-normal-state)
  (setq evil-default-state 'emacs)

  (setq evil-emacs-state-cursor '(box "light blue"))
  (setq evil-cross-lines t)
  (setq evil-move-beyond-eol t)
  (setq evil-move-cursor-back nil)
  (setq evil-want-Y-yank-to-eol nil))

(after! lsp-mode
  (setq lsp-enable-file-watchers nil)
  (setq lsp-clients-clangd-args '("-j=5"
				  "--background-index"
				  "--clang-tidy"
				  "--completion-style=detailed"
				  "--header-insertion=never"
				  "--header-insertion-decorators=0")))

(after! lsp-clangd (set-lsp-priority! 'clangd 2))

(after! magit
  (setq magit-diff-refine-hunk 'all)
  (setq magit-display-buffer-function 'magit-display-buffer-traditional)
  (setq git-commit-summary-max-length 100)

  (defun magit-subup ()
    (interactive)
    (magit-run-git "subup"))

  (defun magit-fetch-develop-to-develop ()
    (interactive)
    (magit-fetch-refspec "origin" "develop:develop" '()))

  (setq magit-branch-adjust-remote-upstream-alist '(
                                                    ("origin/release-22.02" . "22.02-.*")
                                                    ("origin/release-22.06" . "22.06-.*")
                                                    ("origin/release-22.10" . "22.10-.*")
                                                    ("origin/develop" . "RD-.*")
                                                    ))

  (with-eval-after-load 'magit
    (transient-append-suffix 'magit-submodule "s" '("o" "Subup" magit-subup))
    (transient-append-suffix 'magit-fetch "o" '("d" "develop:develop" magit-fetch-develop-to-develop))

    ;; Prevent pushing to upstream (Rebase branch)
    (transient-remove-suffix 'magit-push "u")))

(use-package! consult-projectile
  :config
  (setq consult-buffer-sources (remove 'consult--source-recent-file consult-buffer-sources))
  (add-to-list 'consult-buffer-sources 'consult-projectile--source-projectile-file 'append))

(after! company
  (setq company-idle-delay 0.0))

(after! helm-make
  (setq helm-make-completion-method #'ido))

(after! whitespace
  (setq whitespace-style '(face trailing lines-tail))

  (defun prevent-whitespace-mode-for-magit ()
    (not (derived-mode-p 'magit-mode)))

  (add-function :before-while whitespace-enable-predicate 'prevent-whitespace-mode-for-magit))

(after! vterm
  (setq vterm-shell "/usr/bin/zsh"))

;; ============================= Other tweaks =============================

;; Interface
(setq split-height-threshold nil)
(setq confirm-kill-emacs nil)
(setq fit-window-to-buffer-horizontally t)

;; Common tweaks
(setq-default fill-column 100)
(setq-default tab-width 4)
(setq tab-always-indent t)
;; (setq tab-always-indent 'complete)

;; Global modes
(delete-selection-mode 1)
(global-subword-mode)
(global-auto-revert-mode)
(global-display-fill-column-indicator-mode)
(global-whitespace-mode 1)

;; Emacs
(setq server-socket-dir "/tmp/emacs-servers")

;; Docker
(if (getenv "INSIDE_DOCKER")
    (setq frame-title-format '("%f · Doom Emacs in Docker"))
  (setq frame-title-format '("%f · Doom Emacs")))

;; Format on save
(setq +format-on-save-enabled-modes
      '(not python-mode))

;; ============================= Keymap =============================
(map! :map global-map
      "C-x k" #'kill-this-buffer
      "C-x C-b" #'my/switch-to-other-buffer
      "C-s" #'+default/search-buffer
      "<M-DEL>" #'my/xah-shrink-whitespaces
      "C->" #'mc/mark-next-like-this
      "C-<" #'mc/mark-previous-like-this
      "C-c C-<" #'mc/mark-all-like-this-dwim
      "C-c C-r" #'set-rectangular-region-anchor
      "C-S-x C-S-s" #'my/save-without-hooks)

(map! :after projectile
      :map projectile-mode-map
      "C-S-s" #'+default/search-project
      "C-S-x C-S-f" #'my/project-find-file)

(map! :after magit
      :map magit-status-mode-map
      "q" nil)  ; Prevent quitting magit

(map! :after flyspell
      :map flyspell-mode-map
      "C-;" #'flyspell-correct-at-point)

(map! :after python
      :leader
      :prefix "c"
      "f" #'yapfify-region-or-buffer)
